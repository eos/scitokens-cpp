all: 	rpm

rpm: 
	rm -rf scitokens-cpp
	cp -av jwt-cpp /usr/include/
	git clone https://github.com/scitokens/scitokens-cpp.git
	cd scitokens-cpp; git checkout 838e6c2eccf3e4db069999b73ce427665055d20e ; git archive --prefix "scitokens-cpp-0.4.0/" -o "scitokens-cpp-0.4.0.tar" v0.4.0; git submodule update --init; git submodule foreach --recursive "git archive --prefix=scitokens-cpp-0.4.0/\$path/ --output=\$sha1.tar --output=\$sha1.tar HEAD && tar --concatenate --file=$(pwd)/scitokens-cpp-0.4.0.tar \$sha1.tar && rm \$sha1.tar" ; ls -la ; echo 
	gzip scitokens-cpp/scitokens-cpp-0.4.0.tar
	mkdir -p build/RPMS/
	rpmbuild --define "_rpmdir build/RPMS/" -ta scitokens-cpp/scitokens-cpp-0.4.0.tar.gz
	
