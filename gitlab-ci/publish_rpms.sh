#!/usr/bin/env bash

for arch in "el-6" "el-7" ; do
    EXPORT_DIR=/eos/project/s/storage-ci/www/eos/citrine-depend/${arch}/x86_64/
    echo "Publishing for arch: ${arch} in location: ${EXPORT_DIR}"
    mkdir -p ${EXPORT_DIR}
    cp ${arch}_artifacts/*.rpm ${EXPORT_DIR}
    createrepo -q ${EXPORT_DIR}
done
